---
geekdocBreadcrumb: false
---

{{< hint info >}}
<div style="float:right;">
<iframe title="Mose TV Live - Objectifs 2022" src="https://tube.distrilab.fr/videos/embed/d155daff-3512-4f2c-acb3-9adbfeb7ce9c?title=0&amp;warningTitle=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>
</div>
<div style="background-color:#5555ff;text-align:center;padding:1em 2em;">
<img src="images/logo-live-mose-dev-ondark.svg" width="300">
</div>

L'objectif de Mose TV Live est d'expérimenter l'usage de peertube comme plateforme de streaming professionnel, de voir quels plugins, outils externes ou fonctionnalités peuvent être utilisés pour répondre aux besoins des streameureuses qui travaillent avec du logiciel libre.
<br clear="both">
{{< /hint >}}

Il y a un salon de discussion sur Matrix/Element `#live-mose-dev:matrix.org`

# Programme Mose TV Live

- de janvier à mai 2022, nous avons été présents en live tous les jours (ou presque) histoire de nous confronter à la réalité de l'activité de streaming, sur peertube et sur twitch.
- maintenant nous entrons en phase de construction ou nous organiserons des ateliers à thème, de la production de tutoriels video, le codage d'outils, la mise en place d'espaces collaboratifs et tout ce qui permettra aux streameureuses de se libérer des logiciels et plate-formes privatifs.

{{< hint info >}}
Le streaming a repris le 5 juin 2023, et sera régulier (plus ou moins) les mardis et vendredis de 13h a 15h. N'hésitez pas à venir dire bonjour, ça se passe sur https://tube.distrilab.fr/c/mosetv/videos
{{< /hint >}}
