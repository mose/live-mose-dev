---
title: Outils libres
weight: 300
---

Outils libres pour le streaming
==============================

Twidget
--------------

https://github.com/ZackyTalib/twidget
agregateur de chat twitch, youtube et facebook

Marv
---------

https://github.com/skarab42/marv
app electron destinée a remplacer streamelements

Social Stream
------------------
https://github.com/steveseguin/social_stream
Un aggréateur de chat twitch, youtube, facebook et bien d'autres développé par Steve Seguin, l'auteur de VDO.ninja.


[Outils OBS](https://obsproject.com/forum/resources/categories/tools.4)
==============

- https://obsproject.com/forum/resources/obs-scene-migration-and-verification.1264/ a tester
- https://www.evntboard.io/ plutot recent, a tester
- https://github.com/Sabuto/obs_command-dart CLI pour OBS
- https://obsproject.com/forum/resources/loistosync.1237/ sync audio to video
- https://obsproject.com/forum/resources/obs-video-scheduler.1002/  a tester
- https://obsproject.com/forum/resources/redirect-youtube-live-chat.1215/ mouaif
- https://obsproject.com/forum/resources/animated-lower-thirds-with-dockable-control-panel.1057/ a pomper le principe
- https://github.com/starfishpatkhoo/ytcp a pomper aussi
- https://obsproject.com/forum/resources/zyphens-now-playing-overlay.1026/ qui marche avec tuna
- https://obsproject.com/forum/resources/current-song-overlay-youtube-soundcloud-spotify-vlc-and-more.1064/ qui se plug aussi sur vlc
- https://github.com/Niek/obs-web websocket remote control
- https://github.com/CasparCG/server a voir plus tard
- https://obsproject.com/forum/resources/magic-window.823/ pour zoomer la ou se trouve la souris

Plugins OBS
==============

installé
----------
- https://obsproject.com/forum/resources/move-transition.913/
- https://obsproject.com/forum/resources/advanced-scene-switcher.395/
- https://obsproject.com/forum/resources/infowriter.345/
- https://obsproject.com/forum/resources/multi-source-effect.1412/ pour faire des effets de calque
  - installed `.deb` puis bougé dans `.config`
- https://obsproject.com/forum/resources/scale-to-sound.1336/ change taille d'image selon volume sonore
  - installé par unzip dans le dossier plugins
- https://obsproject.com/forum/resources/obs-text-slideshow.1303/ ticker
  - installed `.deb` puis bougé dans `.config`
- https://obsproject.com/forum/resources/transition-table.1174/ transitions granulaire
  - tres pratique de pouvoir specifier des transitions de scene a scene, par contre l'interface est loin d'etre intuitive. Mais bon ca marche bien...
- https://obsproject.com/forum/resources/gradient-source.1172/ les degrades !
  - super simple, nouvelle source type gradient, avec variation possible d'opacité
- https://obsproject.com/forum/resources/downstream-keyer.1254/ a demystifier
  - ce plugin permet en fait de creer une surcouche a toutes les scenes, activable a volonté,
    pratique par exemple pour une surcouche qui dit qu'on est en pause
- https://obsproject.com/forum/resources/spectralizer.861/

a installer
---------------

- https://obsproject.com/forum/resources/waveform.1423/ un peu mieux que spectalizer
  - `make` errors cf https://github.com/phandasm/waveform
- https://obsproject.com/forum/resources/3d-stinger-transition.1299/ blender aussi non mais 
- https://obsproject.com/forum/resources/obs-pthread-text.1287/ pour enrichir du texte
  - testé l'install mais ca marche po chez moi
- https://obsproject.com/forum/resources/flir-like-lut-gradients-for-the-obs-studio-filters.1280/ jolies couleur
  - quelques png a apppliquer en LUT pour rigoler
- https://obsproject.com/forum/resources/source-copy.1261/ pratique
  - exporte en json des scenes, sources, filtres ou transformations, et importe
- https://obsproject.com/forum/resources/background-removal-portrait-segmentation.1260/ faux fond vert 
- https://obsproject.com/forum/resources/time-warp-scan.1167/ zarb filtre
  - transforme une video en image ligne par ligne c'est chelou
- https://obsproject.com/forum/resources/dynamic-delay.1035/ peut etre utilise pour des effets speciaux  spatiaux
- https://obsproject.com/forum/resources/recursion-effect.1008/ filtre psychedelique
  - ca marche bien ce truc, effet 80's, faut gobber des champis avant
- https://obsproject.com/forum/resources/freeze-filter.950/
- https://obsproject.com/forum/resources/source-switcher.941/
- https://obsproject.com/forum/resources/scrab.845/
  - capture d'ecran a partir d'obs, peut etre pratique pour preparer des scenes
- https://obsproject.com/forum/resources/tuna.843/
- https://obsproject.com/forum/resources/directory-watch-media.801/
- https://obsproject.com/forum/resources/obs-transition-matrix.751/ remplacé par transition table
- https://obsproject.com/forum/resources/streamfx-for-obs%C2%AE-studio.578/
- https://obsproject.com/forum/resources/obs-websocket-remote-control-obs-studio-from-websockets.466/
- https://obsproject.com/forum/resources/multiple-rtmp-outputs-plugin.964/
- https://github.com/royshil/obs-backgroundremoval
- https://obsproject.com/forum/resources/source-record.1285/

a suivre
--------------

- https://obsproject.com/forum/resources/obs-pulseaudio-app-capture.1436/ pour avoir une piste son par app comme avec les browsers
- https://obsproject.com/forum/resources/image-reaction.1342/ change image en fonction du volume sonore
- https://obsproject.com/forum/resources/source-dock.1317/ source dans dock
- https://obsproject.com/forum/resources/droidcam-obs-camera.1308/ phone as webcam
- https://obsproject.com/forum/resources/source-record.1285/
- https://obsproject.com/forum/resources/vnc-source.1000/
- https://obsproject.com/forum/resources/shader-plugins.768/
- https://obsproject.com/forum/resources/replay-source.686/
- https://obsproject.com/forum/resources/input-overlay.552/

HowTo et tutoriels
=====================

en francais
------------

- yosheez
  https://www.youtube.com/channel/UCpaI1FRY8EGK6mEAL7Wfr9w

- zepae (streamer twitch)
  https://www.youtube.com/user/Micaramms

- johan digital solutions
  https://www.youtube.com/channel/UC1VLE5isKBsZbkT2KvLKSLA

- Francois schnell (coding, blender, obs)
  https://www.youtube.com/c/FrancoisSchnell/videos

- zero absolu gaming
  https://www.youtube.com/channel/UCyb-E2cc3iVYS84quhMLseg

en anglais
----------

- Alpha Gaming House
  https://www.youtube.com/c/AlphaGamingHouse

- Sam Woodhall (le techos d'alpha gaming)
  https://www.youtube.com/c/SamWoodhall

- Michael Feyrer Jr
  https://www.youtube.com/c/MichaelFeyrerJr

- Scott Fichter
  https://www.youtube.com/c/ScottFichter

