---
title: Média libres
weight: 200
---

Musique/audio
-----------------

- https://openverse.org/
  photos et sons en CC ou domaine public
- https://backingtrack.gg/ metal since the 80, royalty free always - [sync license](https://backingtrack.gg/Backing_Track_Sync_License.pdf)
- https://pad.april.org/p/libreavous-musiques les musiaques libres du podcast libre-a-vous
- https://musique-libre.org/ archive de musique libre de dogmazic.net
- https://www.auboutdufil.com/index.php?license=CC-BY
- https://www.auboutdufil.com/index.php?license=CC-BYSA
- https://freepd.com/ musique en domaine public
- https://framalibre.org/annuaires/musique
- https://freesound.org petits sons
- https://opengameart.org/ ressources pour faire des jeux
- https://www.dogmazic.net/ service de partage de musique sous licence libre

Video
-----------

- https://www.pexels.com/ videos courtes en domaine public
- https://framalibre.org/taxonomy/term/46 videos libres
- https://www.youtube.com/results?search_query=landscape&sp=EgIwAQ%253D%253D
