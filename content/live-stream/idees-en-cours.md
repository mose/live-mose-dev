---
title: Idées de thémes
weight: 100
---

Ici nous allons garder trace des thèmes évoqués qui pourraient faire l'objet de session spéciales, de streams dédiés ou de dispositifs particuliers. L'ordre est aléatoire dans la liste ci-dessous. Le tout c'est d'en garder trace et ça peut aider a comprendre un peu mieux les intentions qu'il y a derrière tout ce bazar.

Musique libre
-------------

r1 est un ami depuis 25 ans, peut-etre un peu plus, et sa passion c'est la musique libre. Depuis deux decades il pratique cet art sur son temps libre. Pilier de la communauté https://linuxmao.org il publie ses productions sur [soundcloud][r1-soundcloud] et [dogmazic][r1-dogmazic]. J'aimerais bien avec lui entreprendre l'exploration des solutions libres de production musicale. *(mose)*

Peertube livechat live coding
------------------------------

On s'est dit avec John que ca serait cool de faire des sessions de live coding avec John sur le plugin livechat de peertube. On a une longue liste de choses qu'on voudrait y ajouter pour qu'il puisse adoucir la transition du départ de Twitch.

La collaboration avec Davduf en décembre 2022 a donné l'occasion de tester la [montée en charge][octopuce-montee] et une foule d'idée a été transmise à John, l'auteur du plugin peertube de livechat, dont il parle dans [son long post][john-feedback] de janvier.

Quand je suis passé a Paris je l'ai croisé, et depuis on a pris l'habitude de faire des sessions de coding a distance, parce que c'est toujours plus sympa on pourrait très bien en streamer quelques-unes. *(mose)*

Développeurs du monde
-----------------------

J'aimerais bien organiser des rencontres avec des développeurs francophone du continent africain, avec qui je suis en contact depuis peu. Mais il va falloir que je leur demande, et que je réfléchisse a la forme que ca pourrait prendre. *(mose)*

Centre de ressources Streaming
-------------------------------

Il va falloir que je m'attelle à la mise à jour des pages du site live.mose.dev ici présent, notamment pour continuer a lister les ressources libres disponibles aux producteurs de contenus audiovisuels. Quelques liens ont changé, depuis l'an dernier. Et la chasse aux bons coins c'est une activité fastidieuse, mais qui peut être sympa quand on la fait a plusieurs. *(mose)*

Une vie de chatons
-------------------

12b a exprimé son désir de ne pas continuer à faire du streaming de son coté, parce que c'est tout de même pas mal de boulot et il a plein d'autres trucs en cours. J'aimerais bien, du coup, reprendre les idées initiales qu'on avait de donner une visibilité a ce que sont les hébergeurs libres et associatifs, les membres des [Chatons][chatons]. Ca pourrait consister en interviews et en récits de la genèse de divers acteurs du secteur, qu'on envisageait de faire sous le label '[une vie de chatons][vie-de-chatons]'.

Ca inclura des focus sur le Chaton qui nous est propre, le [Distrilab][distrilab], mais aussi sur des projets qui sont nés et portés chez des chatons. Cette dynamique d’hébergement et de gestion d'infrastructure est riche en contributions, en vrai, et j'aimerais bien que ca se sache. *(mose)*


[r1-dogmazic]: https://play.dogmazic.net/artists.php?action=show&artist=6825
[r1-soundcloud]: https://soundcloud.com/erwan-lerale
[octopuce-montee]: https://www.octopuce.fr/test-de-charge-dun-peertube-en-live-avec-auposte/
[john-feedback]: https://www.john-livingston.fr/foss/article/plugin-de-tchat-peertube-retour-rapide-sur-un-test-de-montee-en-charge
[vie-de-chatons]: https://tube.distrilab.fr/c/live_12/videos
[chatons]: https://chatons.org
[distrilab]: https://distrilab.fr
