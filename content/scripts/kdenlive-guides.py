#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import json
import time
import sys

framerate = 30

def parse_timestamp(frame):
    return time.strftime('%-M:%S', time.gmtime(frame/framerate))

def kdenlive(root):
    # Find main playlist block
    main_bin = None
    for child in root:
        if "id" in child.attrib and child.attrib["id"] == "main_bin":
            main_bin = child
            break

    # Search for guides
    guides = None
    for child in main_bin:
        if "name" in child.attrib and child.attrib["name"] == "kdenlive:docproperties.guides":
            guides = json.loads(child.text)
            break

    if not guides:
        return 0

    # Parse guides into correct formate
    for guide in guides:
        print("- [%s](): %s" % (parse_timestamp(guide["pos"]), guide["comment"]))

if len(sys.argv) > 1:
    tree = None
    try:
        tree = ET.parse(sys.argv[1])
    except FileNotFoundError:
        sys.exit('"' + sys.argv[1] + '" does not exist')
    except ET.ParseError:
        sys.exit('"' + sys.argv[1] + '" is not an XML file')

    if len(sys.argv) > 2:
        try:
            framerate = int(sys.argv[2])
        except ValueError:
            sys.exit("Invalid framerate")
    root = tree.getroot()

    file_split = sys.argv[1].split(".")
    if file_split[-1] == 'kdenlive':
        try:
            func = kdenlive(root)
        except KeyError:
            sys.exit('"' + sys.argv[1] + '" is an invalid file type')
    else:
        sys.exit('"' + sys.argv[1] + '" is unknown file type')
else:
    sys.exit("No file to parse")
