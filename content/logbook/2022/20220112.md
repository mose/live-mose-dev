---
title: Mercredi 12 janvier 2022
weight: -20220112
---

Matin
-----------

Pour régler mon soucis d'encodage il faut comprendre que j'étais en encodage logiciel parce que des que je passait en encodage matériel (avec ma carte nvidia 1080), OBS refusait de streamer, prétextant qu'il n'avait pas assez de mémoire disponible.

En tuant firefox, par contre, ca marchait bien. C'est plus tard que tytan m'a signalé que firefox activait l'usage de la carte graphique par défaut. Ca explique pas mal de choses: plus j'avais de fenêtres firefox ouvertes avec de la video dedans, plus il réservait de la mémoire de la carte graphique.

J'ai donc désactivé l'accélération matérielle dans firefox et utilisé les paramétrer avancés dans OBS pour augmenter le débit (de 5000 a 12000) et changer quelques paramètres. Du coup c'est vachement plus fluide niveau encodage, maintenant que l'encodage OBS passe par nvenc. Mais pour être sérieux ca vaudrait le coup que je me procure une carte graphique plus récente. C'est juste qu'en ce moment, acheter une carte graphique, c'est pas forcement une bonne idée, vu la pénurie.

Dans ma lancée j'ai aussi redesign le titre de la chaîne en live mose dev, comme ca ca correspond au nom de domaine, j'ai utilisé le logo bricolé la veille pour refaire toutes les images, renommé le repo sur codeberg, changé le pointage dns en conséquence, reporté les changements partout ou je le voyais nécessaire. Pendant quelques heures du coup le site était inaccessible, mais tout est maintenant rentré dans l'ordre.

14h-17h - discussion ouverte
------------------------------

J'avais invité booteille à passer nous voir pour discuter, et il est passé pour l'inauguration de ce nouveau format de l'après-midi. On a causé de plein de trucs avec tytan, et puis shinobi est passé faire coucou et sur la fin greencoder nous a parlé de clojure.

On a causé :

- de [Framasoft](https://framasoft.org)
- de [Codeberg](https://codeberg.org)
- de [Gitea](https://gitea.io/) [dans le fediverse](https://framapiaf.org/web/statuses/107576792068279994)
- des besoins de contenus francophone
- du but de live.mose.dev
- de la traduction de [krita](https://krita.org/fr/)
- de [techlore](https://techlore.tech/)
- et de leur [playlist sur le thème de la securité et de l'anonymité](https://www.youtube.com/playlist?list=PL3KeV6Ui_4CayDGHw64OFXEPHgXLkrtJO) et du besoin d'avoir un truc comme ca en français
- du principe d'utiliser les lives comme des rushes
- du besoin d'avoir un truc qui fait comme [watch2gether](https://w2g.tv/) en libre
- de [vdo.ninja](https://vdo.ninja)
- de [socialstream](https://socialstream.ninja)
- de [galene](https://galene.org/)
- de [owncast](https://owncast.online/)
- de [peertube](https://joinpeertube.org/) et [chocobozzz](https://framapiaf.org/@Chocobozzz)
- [piped](https://github.com/TeamPiped/Piped) comme alternative a invidious
- des [replays dans le livechat peertube](https://tube.distrilab.fr/w/tXtG7Au6jykZvq6f9qb2UV)
- des outils OBS
- des plateformes de paiement contributif liberapay tipee et autres
- des modèles économiques du libre
- de caisses mutualistes federées autour de peertube
- du critère de valeur des productions videos en marge du système capitaliste
- de la maintenance du projet peertube
- des mécaniques de motivation du libre et le pouvoir du merci 
- sous-titres automatiques dans OBS et [commonvoice](https://commonvoice.mozilla.org/fr)
- urls des videos dans peertube
- les humains normaux font pas control-e
- les [tisseurs](https://tisseurs.org)
- [easter eggs](https://www.easter-eggs.com/) et les coopératives
- [onestla.tech](https://onestla.tech) et l'educpop sur les coopératives
- et y'a des gauchistes chez ubisoft
- la cooperative [motion twins](https://motion-twin.com/fr/) qui font des jeux
- et puis [codeurs en liberté](https://www.codeursenliberté.fr/)
- et [la parisienne liberée](https://www.laparisienneliberee.com/)
- zimmerman qui fait des chansons ([rien à cacher](https://piped.kavin.rocks/watch?v=rEwf4sDgxHo))
- invidious plus trop maintenu
- [freetube](https://freetubeapp.io/)
- [privacy redirect](https://addons.mozilla.org/fr/firefox/addon/privacy-redirect/)
- [lufi](https://linuxfr.org/news/un-nouveau-logiciel-libre-lufi) pour les échanges de fichiers
- l'idée d'utiliser [webcrypto pour lufi de booteille](https://framagit.org/fiat-tux/hat-softwares/lufi/-/issues/185)
- kudos à [linuxfr](https://linuxfr.org/)
- greencoder et [clojure](https://clojure.org)
- [applis clojure](https://github.com/razum2um/awesome-clojure)
- [penpot](https://penpot.app/) qui est un genre de figma en libre
- projet activitypub en clojure de greencoder
- [fediverse.party](https://fediverse.party)
- tytan a toujours la réponse

