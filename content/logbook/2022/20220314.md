---
title: Lundi 14 Mars 2022
weight: -20220314
---

Alors, ca fait un moment que je me dis que l'intro du live est trop longue. Et peut-etre que la musique de 30s pourrait commencer au début du countdown. Mais ca veut dire qu'il y a besoin d'un peu d'automatisation, essayons de faire ca avec [advanced scene switcher](https://obsproject.com/forum/resources/advanced-scene-switcher.395/).

Donc, j'ai une scène pre-intro, qui a un compte à rebours de 15 secondes, puis une scène intro qui montre la video de la musique, puis quand la video finit ça switch sur la scène fullcam. 

![shoot scenes](../images/2022-03-15-01-07_grab.png)

Ce que je veux c'est que la video commence au début de `pre-intro` quand je change de scène, mais qu'on la voie pas, puis quand le compte à rebours atteint sa fin (et que donc il affiche `now`), on passe à la scène `intro`. Mais faut que la video continue sur sa lancée. Il y une case à cocher pour la source media pour spécifier que le media doit redémarrer, donc ne cochons pas cette case.


La configuration du script

![script countdown](../images/2022-03-15-01-24_grab.png)

Maintenant, je crée deux macros, une se déclenche au début quand j'active la scène `pre-intro`, cette action redémarre la video `media source 3`, sur laquelle j'ai mis un filtre `solor correction` qui mets l'opacité a zéro.

![macro reset pre-intro](../images/2022-03-15-01-26_grab.png)

La seconde macro se déclenche dès que le compte a rebours a fini, et du coup affiche le `now`. Du moment ou ce texte apparaît, on désactive le filtre d'opacité sur la video, et on passe a la scène `intro`. Du coup la video continue a jouer la musique en continuité. Ca a l'air simple comme ça mais ca m'a pris un moment et plein de tests successifs pour y arriver.

![macro pre-intro](../images/2022-03-15-01-27_grab.png)

(oui, dans mes screenshots il y a une typo, c'est pas per-intro mais pre-intro, je sais)