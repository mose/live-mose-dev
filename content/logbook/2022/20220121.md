---
title: Vendredi 21 janvier 2022
weight: -20220121
---

14h - 17h - discussion commune
---------------------------------

Alors aujourd'hui quand je suis arrivé dans vdo.ninja/mose pour démarrer la diffusion, il y avait deja tytan et greencoder qui discutaient a bâtons rompus. C'était une drôle d'expérience que de leur demander de se taire pour que je puisse lancer le machin.

En fait, cette coulisse est devenu un lieu partagé qui dépasse son role initial. Et ce détournement est une proposition dont il va falloir discuter: c'est super intéressant de considérer le dispositif de streaming comme un commun! Et encore plus de se demander quel outillage pourrait mieux supporter cette approche.

Vers la fin j'ai eu le même sentiment. Je voulais clore la session mais visiblement shinobi et tytan étaient bien lancés, j'ai du les couper pour fermer, je ne devrais pas avoir a le faire. Je voudrais bien trouver moyen de dire: bon, allez j'y vais, éteignez la lumière quand vous avez fini. Faudra que je pose ça sur la tapis le prochain coup.

Bon, sinon on causait du projet-expérience de greencoder qui veut faire un système de gestion d'inventaire fédéré a base d'[activitypub](https://www.w3.org/TR/activitystreams-vocabulary/#activity-types) pour de la gestion de frigo. Mon avis était que y'avait peut-êtres d'autres lieux d'inventaires fédérés qui seraient plus appropriés comme les ressourceries, quand subreptice, qui nous écoute papoter assez régulièrement, nous a parlé dans le chat des [frigos solidaires](https://www.linfodurable.fr/social/gaspillage-alimentaire-ou-trouver-des-frigos-solidaires-pres-de-chez-soi-18906).

Curieux, j'allais lancer un matage de video sur le sujet dans w2g, quand tytan m'a signalé la nouvelle fonctionnalité dans vdo.ninja qui pouvait faire pareil (visionnage synchro de vidéos youtube). Il arrête pas de nous esbroufer le steve.

![](../images/vdo-website-request.png)

![](../images/vdo-website-announce.png)

Alors du coup on a pris le temps de tester sur le vdo.ninja beta et ça marche bien. Le seul truc c'est que la synchro ne marche qu'avec youtube, va falloir qu'on voie a faire marcher ca avec les vidéos peertube aussi tiens (et les piped et invidious aussi). Ca a été aussi l'occasion de prendre connaissance de la [doc de vdo.ninja](https://docs.vdo.ninja/) (au bout d'un moment il était temps).

On a donc regardé la première [video des frigos solidaires](https://piped.kavin.rocks/watch?v=un97XEvCb88) et une autre sur sa propagation a grenoble. Sympa. Au final, le frigo est peut-etre moins individualiste qu'on aurait pu le supposer.

A un moment on a aussi parlé de [revolt](https://revolt.chat/), une alternative libre a discord, mais qui est vraiment une copie conforme, jusqu'a reproduire le modèle du serveur centralisé. De nos jours, ne pas penser federation nous semble a tous une erreur, les serveurs centralisés pour ce type de services sont de trop évidents maillons faibles. Par contre [fosscord](https://fosscord.com/), lui, est fédéré, et en plus peut se connecter sur discord pour faciliter une transition.
