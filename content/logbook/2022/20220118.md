---
title: Mardi 18 janvier 2022
weight: -20220118
---

Alors je commençais a faire le montage avec les rushes de jeudi dernier, et j'ai vu passer quelques pouets qui ont detourné mon attention. Dachary annonce le [webinaire de federation de forge](https://forum.forgefriends.org/t/forge-federation-webinar-january-19th-2022-10am-noon-utc-1/538) demain, c'est un peu de ça qu'on parlait la semaine dernière à propos de Gitea. Quand il a vu que je m'y intéressais il m'a demandé si je pouvais l'aider pendant la séance a poser une question en chinois pour valider le principe de fonctionnement de [webinar jitsi avec traducteur synchrone](https://bittube.video/w/3f604ed9-af7e-4313-b17f-6054a0ace564). Bon, je me débrouille un peu en chinois mais j'ai préféré aller demander a des copains s'ils pourraient jouer le jeu. L'un d'eux, mon vieux pote xenor, a dit qu'il viendrait. Cool !

14h-17h - discussion ouverte
------------------------------

Avec tytan et shinobi on a causé un peu de ce webinaire demain. Il est probable que je couvre l'evenement dans un live special, c'est a 10h du mat pour moi c'est pratique ca fait 17h de mon coté.

Ensuite j'ai pris le temps de chouiner a propos de ma carte graphique, je me suis rendu compte que j'avais pas une 1080 mais une 1050 avec 2G de ram. Pas étonnant que je galère. On a papoté de cartes graphiques, on a été voir un peu sur un site taiwanais de vente en ligne qu'est-ce qu'il y avait de dispo et quels étaient les prix. En fait il y a peu de différence de prix entre taiwan et la fronce.

J'ai aussi raconté le fait qu'en allant brancher mes potes devs taiwanais y'en a un qui m'a demandé si j'y connaissais quelque chose en web3. J'ai filé le lien vers [web0](https://web0.small-web.org/) pour lui demander s'il connaissait, en réponse. Et avec tytan et shinobi on a un peu causé du [post de moxie sur le web3](https://moxie.org/2022/01/07/web3-first-impressions.html), se revoltant de son postulat que les gens voulaient pas avoir de serveur chez eux. En fait, les nft et les blockchains, on est tous un peu d'accord pour dire que c'est du purin, pour des raisons autant techniques qu'idéologiques.

A un moment je sais pas trop pourquoi on en est venu à se detendre en allant voir [rouille](https://github.com/bnjbvr/rouille) et [marcel](https://github.com/brouberol/marcel), si vous connaissez pas ca vaut le détour.

Puis j'ai raconté comment s'était passée la [reunion chatons](https://mypads.framapad.org/p/2022-01-reunion-virtuelle-mensuelle-yrga97vf) de la veille au soir. On a pris le temps de matter https://stats.chatons.org en détail, qui est un excellent outil de mise en visibilité pour les chatons.

C'était encore une fois un bon moment, même si c'était un peu technique par moment, et subreptice, qui nous a fait le plaisir de nous rendre visite, était parfois un peu perdu :)