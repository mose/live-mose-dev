---
title: Samedi 8 Janvier 2022
weight: -20220108
---

<s>7h</s> 8h - retrospect
------------------------------

Un peu en retard sur le programme, je mets à jour le log d'hier et je refléchis à cette possibilité de faire des best-of hebdomadaires. Et là, Tytan me parle d'un [plugin InfoWriter](https://obsproject.com/forum/resources/infowriter.345/) qui permet d'enregistrer des markers lors d'un recording dans OBS. Du coup je l'installe et ça marche du tonnerre !

<s>12h</s> 11h - stream pro
---------------------------------

On finit de lister les outils OBS interessants, vers la fin y'en avait de moins en moins parce que c'était par ordre d'ancienneté du coup les vieux outils de 2014 avaient un peu perdu de leur utilité. Donc bon maintenant on a une bonne liste d'outils a tester plus tard.

17h - outils libre
---------------------

on commencé le passage en revue de tous les plugins OBS avec tytan652 qui connait, en plus. La liste des plugins qu'il faut que j'installe s'allonge a grande vitesse.

21h - open bar
------------------

Alors j'avais prévu de me reposer un peu le weekend et donc samedi et dimanche de zapper ce dernier creneau. Mais shinobi s'est pointé et m'a demandé si je connaissais https://github.com/steveseguin/social_stream

C'est un projet d'aggregation de live chat par l'auteur de vdo.ninja (voir [video de presentation](https://www.youtube.com/watch?v=X_11Np2JHNU)) qui a laair bien cool.

Du coup j'ai lancé le live et on a papoté pendant 3 heures. Pas facile de faire des pauses en fait.

On a parlé aussi de la [parisienne liberée](http://www.laparisienneliberee.com/) qui fait des super musiques, du bouquin ['terre et liberté' d'Aurelien Berlan](https://www.partage-le.com/2021/11/16/a-propos-de-terre-et-liberte-daurelien-berlan/) et de divers sujets en rapport avec la politique et l'anarchisme.


