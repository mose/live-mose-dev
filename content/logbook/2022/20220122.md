---
title: Samedi 22 janvier 2022
weight: -20220122
---

14h - 17h - papotages
-------------------------

Alors d'abord on a noté que Frame venait de liberer le [code de leur firmware](https://frame.work/blog/open-sourcing-our-firmware) (comment on dit ça en français ?) et que c'était trop une bonne nouvelle.

Et puis j'ai parlé de ce que j'ai glané sur le canal irc de l'april concernant toutes les [musiques libres](https://pad.april.org/p/libreavous-musiques) qu'ils utilisent dans le podcast hebdomadaire [libre a vous](https://www.libreavous.org/) (que franchement je vous conseille).

Par ailleurs j'ai parlé de cette idée d'avoir un outil qui permette de collectiviser le controle a distance d'OBS. Tytan en a profité pour signaler que dans la 27.2 OBS allait étendre l'API javascript du [navigateur embarqué](https://github.com/obsproject/obs-browser), ce qui pourrait offrir quelques alternatives a l'usage des websockets pour piloter OBS à distance (ou du moins des couches qui y sont intégrées comme ça se fait deja beaucoup). Mais tout de même le [CEF (chrome embedded framework)](https://bitbucket.org/chromiumembedded/cef/) c'est un gros bousin lourdingue. Tiens au fait y'a un début d'alternative a electron qui s'appelle [Tauri](https://tauri.studio/), c'est une bonne nouvelle.

En passant on a causé de [Searx](https://github.com/searx/searx) et les liens suivant je les cherchais sur [celui d'underworld](https://searx.underworld.fr/). Ce moteur de recherche distribué est la depuis un moment, et on oublie trop souvent qu'il existe.

Subreptice, qui nous rend visite quasi tous les jours, nous a parlé de ce qu'il avait reçu pour ses projets d'[auto-hébergement](https://yunohost.org/fr/selfhosting): un [boitier argon 40](https://www.argon40.com/) qui utilise un raspberry 4 avec des ports SATA pour y coller du disque et faire un NAS. C'est joli ce truc.

Du coup on a causé de [proxmox pour desktop](https://www.reddit.com/r/Proxmox/comments/q53rp5/using_proxmox_on_desktop/) (sujet qui intéresse shinobi, parce qu'il veut l'utiliser pour faire du [GPU passthrough](https://www.portegi.es/blog/proxmox-gpu-streaming)) et on a découvert qu'il y avait un packaging de prixmox spécial raspberry appelé [pimox](https://github.com/pimox/pimox7).

Sinobi nous a aussi parlé de la d'ou venait [o3de](https://www.o3de.org), qui vient proposer une alternative de moteur de jeu en libre (face a unity ou unreal). Faudrait tester ça.

Pour son auto=-hebergement, subreptice nous parle de [Netlibre](https://netlib.re/) pour avoir une nom de domaine gratos (en fait un sous-domaine). Intéressant cette initiative du [FAI ARN](https://www.arn-fai.net/) (affilié a la FFDN).

En vrac on a aussi causé des [termes français officiels publiés dans le journal officiel](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000029461191/) et aussi de [MiNT](https://fr.wikipedia.org/wiki/MiNT) et de [freemint](https://freemint.github.io/).

Sur la fin j'ai commencé a me dire que c'était vachement cool de passer notre temps de papoter à la sauvage, mais niveau objectif de ce stream on était un peu a coté. Il va falloir recadrer un peu sur la semaine qui vient pour qu'on puisse commencer a publier du contenu en français pour vulgariser les outils libres de streaming.

On va probablement continuer a utiliser le live comme un genre de coulisse, pour préparer des demos, répéter un peu, puis passer en mode 'on air', enregistrer de bout en bout une séquence, comme ça je peux ensuite l'extraire d'un bloc dans kdenlive et l'upload direct sans avoir a faire de montage. Loi du moindre effort ftw.
