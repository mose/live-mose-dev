---
title: Mercredi 5 Janvier 2022
weight: -20220105
---

7h - matinouillale
-------------------

Je me suis levé un peu en retard, final j'ai mis la matinale d'Ostpolitik https://www.twitch.tv/ostpolitik qui lui est toujours a l'heure et puis c'est un pro. Pendant ce temps j'ai bouffé puis mis a jour le logbook ici present. De toute facon ce creneau horaire je sens qu'il va sauter y'a personne. Enfin bon on verra apres quelques jours.

12h - strimpro
---------------

Avant tout il fallait quand meme que je règle ce soucis d'ouverture de fichier dans mon OBS installé avec flatpak. Heureusement tytan652 est passé, lui il s'y connait en trucs bas niveau, et il m'a mis sur des pistes intéressantes. J'ai tenté d'installer des machins dans flatpak, genre libs qt5 mais sans succes.

Finalement j'ai compris que flatpak, dans mon cas, c'etait vraiment pas adapté, j'ai compil OBS a l'ancienne et pis du coup bin voila ca marchait ... aux chiottes flatpak avec ta fausse isolation.

Apres avoir recopié les confs dans `.config/obs-studio` et y avoir ajouté les plugins [move transition](https://obsproject.com/forum/resources/move-transition.913/) et [advanced scene switcher](https://obsproject.com/forum/resources/advanced-scene-switcher.395/)

17h - outils libres
-------------------------

Maintenant que j'ai un OBS qui tourne sans soucis, j'ai bricolé un peu avec tytan et john qui est passé dire bonjour. L'option qui dans vdo.ninja n'affiche que celui qui parle est tres bien, mais ne marche pas avec la scene 1, il faut utiliser la scene 0. Avec la custom CSS de l'inclusion des videos sur le label du nom de qui parle, ca rend vachement bien.

```
.video-label {
    color: #fff;
    font-family: "Quicksand";
    font-weight: 700;
    border-radius: 0 2em 0 0 ;
    padding: .2em 1.5em .2em .5em;
    margin: 0;
    background: rgba(0, 0, 0, 0.8);
}
```

Apres je suis retourné voir pourquoi mes stingers étaient pas transparents. D'abord pour etre sur, j'ai download des stinger en ligne, de [Quince-media](https://quincemedia.com/?s=transitions), tres game classe. Et du coup ca marchait bien, le probleme n'etait pas dans OBS, plutot dans kdenlive ... j'y ai passé des heures, c'est incomprehensible.

Entre temps shinobi s'est pointé et on a causé du fait que ca serait top cool de faire un beuf en ligne dans un live. C'est chaud a cause de la latence qui nique la synchro mais y'a des feintes. [Superbe article dans linux-fr à ce sujet](https://linuxfr.org/users/thecross/journaux/jouer-avec-son-groupe-a-distance).

21h - open bar
-------------------

Arrivé une heure en retard j'en ai profité pour mettre a jour ce logbook. J'etais en fait peinard, bien lancé, j'ai refait le logo de la chaine, bricolé les scenes et les sources. Pis j'ai ajouté des noms de participants au live dans l'outro.

Et au moment ou j'ajoutais John dans les credits avec le titre de maitre-chat, il se pointe. Coincidence ? Du coup on a papoté sur plein de trucs, comme le role de l'implication dans le logiciel libre dans une prise de conscience politique, les idées anarcho-communistes qui nous semblent en decouler, et l'importance des reseaux humains dans la resilience des communauté contributives du libre, entre autres choses. On etait un peu tout seul mais cette discussion etait tres stimulante !

En tout cas pour le moment je ne vois que de bonnes raisons pour poursuivre cette experience de streaming intensif. Je suis convaincu que quelque chose de positif et de (r)evolutionnaire en decoulera (ou au moins ca y participera).

