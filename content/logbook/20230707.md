---
title: Vendredi 7 Juillet 2023
weight: -20230707
---

L'avenir du Livechat peertube
--------------------------------

Après la discussion avec DavDuf de [Au Poste][1] de hier soir, on parle un peu des évolutions prévues pour le livechat peertube, brique essentielle de la construction de communautés autour de la production de contenus video en direct (le streaming, comme on dit an angliche).

Il y a pas mal de truc dans la liste des choses en vue, et ce qui est chouette c'est que les gens qui bossent avec David (Nico et Olivier) sont chauds pour participer. En plus on va avoir, si on se débrouille bien, du financement de diverses sources pour assumer la charge de travail (nlnet, mais davduf a aussi parlé de financnements qu'il pourrait avoir de son coté, et il y a aussi des restes de fonds de framasoft pour des développements en cours).

On etait pas mal nombreux dans vdo.ninja parce que John a poukave sur mastodon. Mais le résultat au final etait plutot bien.

[1]: https://auposte.fr