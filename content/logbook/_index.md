---
title: Carnet de route
---

A chaque jour son stream. Mais vu que je ne publie pas les enregistrements, c'est bien pratique de garder une trace de nos ébats sous forme écrite.

Dans ce carnet de route je vais conserver mention chaque jour de ce dont on a parlé, des liens qu'on s'est échangé, et ce genre de choses. Ca sera plus facile, au moins pour les participants, d'utiliser le petit moteur de recherche pour retrouver des trucs dont on a parlé.
