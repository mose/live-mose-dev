---
title: Mardi 11 Juillet 2023
weight: -20230711
---

Un peu de ménage sur le site web
----------------------------------

Bon, j'ai quelques entrées du journal de bord en retard, on va se mettre à jour. Mais par contre, j'ai eu un soucis de montée en charge handicapante quand j'ai commencé a partager mon écran dans vdo.ninja. Du coup dans vscode c’était l'enfer pour taper des choses, y'avait un gros lag et les caractères s'affichaient tout mélangés.

Il va falloir que je teste un peu plusieurs façons de gérer ca, peut-être réduire un peu la resolution, ou bien passer plutôt par un partage séparé du partage de la camera. De toute façon la prochaine session je compte partager un écran aussi donc on verra bien.

