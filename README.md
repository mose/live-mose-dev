# mose-tv-live

Notes de live de https://tube.distrilab.fr/w/97iACprK1R87G7dcJZg8N2

- [Programmation](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/live-stream/programmation.md)
- [Ressources](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/live-stream/ressources.md)

Carnet de route

- [Mardi 4 janvier](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/logbook/20220104.md)
- [Mercredi 5 Janvier](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/logbook/20220105.md)
- [Jeudi 6 Janvier](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/logbook/20220106.md)
- [Vendredi 7 Janvier](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/logbook/20220107.md)
- [Samedi 8 Janvier](https://codeberg.org/mose/mose-tv-live/src/branch/main/content/logbook/20220108.md)
