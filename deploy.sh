#!/usr/bin/env bash

rm -rf public

hugo

cp .domains public

git add .
git commit -a -m'deploy'
git push

git subtree push --prefix=public git@codeberg.org:mose/live-mose-dev.git pages
